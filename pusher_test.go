package Pusher

import (
	"testing"
)

func TestEbody(t *testing.T) {
	p := NewClient("3", "278d425bdf160c739803", "7ad3773142a6692b25b8")

	expected := `{"name":"foo","channels":["project-3","project-4"],"data":"{\"some\":\"data\"}"}`
	p.jsonifyData("foo", "{\"some\":\"data\"}", []string{"project-3", "project-4"})

	if expected != string(p.body[:]) {
		t.Errorf("json error\n%s\n%s", expected, string(p.body[:]))
	}
}

func TestPost(t *testing.T) {
	p := NewClient("3", "278d425bdf160c739803", "7ad3773142a6692b25b8")
	b, err := p.post([]byte("helloworld"), "http://www.baidu.com")
	if err != nil {
		t.Errorf("Post error:\n%s\n%s", b, err)
	}
}

func TestTriggerUrl(t *testing.T) {
	p := NewClient("3", "278d425bdf160c739803", "7ad3773142a6692b25b8")
	p.timestamp = MockTimestamp
	p.jsonifyData("foo", "{\"some\":\"data\"}", []string{"project-3"})

	expected := "http://api.pusherapp.com/apps/3/events?auth_key=278d425bdf160c739803&auth_timestamp=1353088179&auth_version=1.0&body_md5=ec365a775a4cd0599faeb73354201b6f&auth_signature=da454824c97ba181a32ccc17a72625ba02771f50b50e1e7430e47a1f3f457e6c"
	res := p.TriggerUrl()

	if expected != res {
		t.Errorf("Trigger URL error:\n%s\n%s", expected, res)
	}
}

func TestPublish(t *testing.T) {
	p := NewClient("80759", "77ef11e81e23aefafcdc", "350a76de04a9d52859d7")
	err := p.Publish("my_event", "{\"message\":\"hello world\"}", "test_channel")

	if err != nil {
		t.Errorf("Publish error", err)
	}

}

func MockTimestamp() string {
	return "1353088179"
}
