package Pusher

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"
)

type Pusher struct {
	app_id      string
	auth_key    string
	auth_secret string
	body        []byte
	host        string
	timestamp   NormalFunc
}

type EbodyData struct {
	Name     string   `json:"name"`
	Channels []string `json:"channels"`
	Data     string   `json:"data"`
}

type NormalFunc func() string

func (p *Pusher) Publish(name, data string, channels ...string) error {
	err := p.jsonifyData(name, data, channels)

	body, err := p.post(p.body, p.TriggerUrl())

	fmt.Printf("Body:%s", body)
	if err != nil {
		return err
	}
	return nil
}

func (p *Pusher) post(content []byte, postUrl string) (string, error) {
	buffer := bytes.NewBuffer(content)
	resp, err := http.Post(postUrl, "application/json", buffer)

	if err != nil {
		return "", fmt.Errorf("post error:%s", err)
	}

	defer resp.Body.Close()

	pBody, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return "", fmt.Errorf("Body parse error:%s", err)
	}

	if resp.StatusCode != 200 {
		return "", fmt.Errorf("Not 200 OK:%s", pBody)
	}

	return string(pBody), nil
}

func (p *Pusher) jsonifyData(name string, data string, channels []string) error {

	e := &EbodyData{
		name,
		channels,
		data,
	}
	res, err := json.Marshal(e)
	if err != nil {
		p.body = []byte("")
		return err
	}
	p.body = res
	return nil
}

func (p *Pusher) TriggerUrl() string {
	format := "http://%s%s?%s"

	sig := &Signature{
		p.auth_key,
		p.auth_secret,
		"",
		p.timestamp(),
		"1.0",
		p.body,
		p.EventPath(),
	}

	return fmt.Sprintf(format, p.host, p.EventPath(), sig.path())
}

func (p *Pusher) EventPath() string {
	format := "/apps/%s/events"
	return fmt.Sprintf(format, p.app_id)
}

func NewClient(app_id, auth_key, auth_secret string) *Pusher {
	return &Pusher{
		app_id,
		auth_key,
		auth_secret,
		[]byte(""),
		"api.pusherapp.com",
		GenTimestamp,
	}
}

func GenTimestamp() string {
	return strconv.FormatInt(time.Now().Unix(), 10)
}
