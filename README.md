pusher-golang-server
===

[![Build Status](https://travis-ci.org/superalsrk/pusher-golang-server.svg?v=2)](https://travis-ci.org/superalsrk/pusher-golang-server)
[![Build Status](https://drone.io/github.com/superalsrk/pusher-golang-server/status.png?v=2)](https://drone.io/github.com/superalsrk/pusher-golang-server/latest)

[Pusher](http://pusher.com)'s golang library for server,[godoc](http://godoc.org/github.com/superalsrk/pusher-golang-server)

##Install##

```bash
$ go get github.com/superalsrk/pusher-golang-server
```

##Usage##

```go
package main

import (
    "github.com/superalsrk/pusher-golang-server"
)

func main(){
    p := NewClient("80759", "77ef11e81e23aefafcdc", "350a76de04a9d52859d7")
    err := p.Publish("my_event", "{\"message\":\"hello world\"}", "test_channel")

    if err != nil {
        t.Errorf("Publish error", err)
    }
}
```

##License##
MIT